console.log(document);
console.log(document.querySelector("#txt-first-name"));
console.log(document.getElementsByClassName("txt-inputs"));
console.log(document.getElementsByTagName("inputs"));

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(spanFullName);

txtFirstName.addEventListener("keyup", printFullName);
txtLastName.addEventListener("keyup", printFullName);

function printFullName(event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
};

const labelFirstName = document.querySelector("#label-first-name");
console.log(labelFirstName);



